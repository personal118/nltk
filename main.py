from logging import log
from nltk import tokenize
from nltk.data import retrieve
from sklearn import metrics
from utils import get_logger
import pandas as pd
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.datasets import make_blobs, make_circles, make_moons
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.probability import FreqDist
from nltk.stem import PorterStemmer
from sklearn.metrics import mean_squared_error

logger = get_logger("nltk")

class Text:
	def __init__(self, content, name, lang) -> None:
		self.content = content
		self.name = name
		self.lang = lang
def stemm_word_tokens(tokens, lang):
	ps = PorterStemmer()
	stemmed_words=[]
	for w in tokens:
		stemmed_words.append(ps.stem(w))
	return stemmed_words

def clean_word_tokens(tokens, lang):
	stop_words = stopwords.words(lang)
	stop_words += [".", ",", ":", ";", "!", "?", "'", "''", "\"", "'s", "…"]

	cleaner_tokens = []
	for token in tokens:
		if token not in stop_words:
			cleaner_tokens.append(token)

	return cleaner_tokens

def plot_freq_words(word_tokens, name):
	df = pd.DataFrame()
	df["word"] = np.array(word_tokens)
	sns.countplot(x="word", data=df, palette="Set3", order = df['word'].value_counts().iloc[:40].index)
	plt.xticks(rotation=90)
	plt.tight_layout()
	plt.savefig(f"{name}.png")
	plt.clf()
	return

def text_processing(text):
	logger.info(f"Tokenize {text.name}... ")
	sentence_tokens = sent_tokenize(text.content)
	word_tokens = word_tokenize(text.content)
	logger.info(f"Sentence tokens count = {len(sentence_tokens)}")	
	# logger.info(f"Sentence tokens: {sentence_tokens}")
	logger.info(f"Word tokens count = {len(word_tokens)}")
	# logger.info(f"Word tokens: {word_tokens}")
	logger.info(f"Count of word tokens after tokenize: {len(word_tokens)}")
	logger.info(f"Count of unique words after tokenize: {len(pd.DataFrame(np.array(word_tokens))[0].value_counts())}")
	plot_freq_words(word_tokens, f"{text.name}_freq_word_after_tokenize")


	logger.info("Removing stop words...")
	cleaned_word_tokens = clean_word_tokens(word_tokens, text.lang)
	plot_freq_words(cleaned_word_tokens, f"{text.name}_freq_word_after_clean")
	logger.info(f"Count of unique words after clean: {len(pd.DataFrame(np.array(cleaned_word_tokens))[0].value_counts())}")


	logger.info("Stemming words...")
	stemmed_words = stemm_word_tokens(cleaned_word_tokens, text.lang)
	plot_freq_words(stemmed_words, f"{text.name}_freq_word_after_stemm")
	logger.info(f"Count of unique words after stem: {len(pd.DataFrame(np.array(stemmed_words))[0].value_counts())}")

	prob = pd.DataFrame(pd.DataFrame(np.array(stemmed_words))[0].value_counts())
	prob[1] = prob[0] / len(stemmed_words)
	prob = prob.reset_index()

	cipf = []
	cipf.append(prob[0][0])
	for rang in range(2, len(prob)+1) :
		cipf.append(cipf[0]/ rang)

	prob = prob.rename(columns={0: 'count', 1: 'prob', "index": "word"})
	prob["cipf"] = np.array(cipf) / len(stemmed_words)
	mse = mean_squared_error(prob["prob"], prob["cipf"])

	return prob, mse





def main():
	# nltk.download()
	# nltk.download('punkt')
	# nltk.download('stopwords')

	texts = []
	texts.append(Text('''
New research on dinosaur footprints unearthed in Spain adds to growing evidence that a dinosaur that was genetically similar to the Tyrannosaurus rex was extremely agile. The Tyrannosaurus rex was a very large meat-eating dinosaurs that was not agile.The findings were published on December 9 in Scientific Reports, which covers natural sciences. The findings announced the existence of sets of fossilized dinosaur footprints that prove the dinosaur could move quickly.These footprints join other sets found in Utah and Texas. One of the sets there shows dinosaurs running at speeds over 48 kph. The Spanish footprints showed speeds of nearly 45 kph.To figure out the running speed, scientists measured the length of the footprints and then considered the height of the place where the dinosaur legs meet the body, or the hip. They also needed to consider the distance between one footprint and another on the same foot.All the known sets of prints that show speed come from a family of dinosaurs called theropods. These carnivorous, or meat-eating, dinosaurs stood on two legs and could not fly. They are like the famed velociraptor, a dinosaur seen in many famous movies.The researchers estimated that the animal that created the most recent set of footprints was probably 1.5 to 2 meters tall and 4 to 5 meters long from mouth to tail.Scientists think there may be faster dinosaurs, but these footprints have been easier to find. The footprints are known as tracks when there are one or more long sets.“Behavior is something very difficult to study in dinosaurs,” said lead writer Pablo Navarro-Lorbés of the University of La Rioja. These kind of findings are very important, I think, for improving that kind of knowledge.Scientists usually predict dinosaur behavior through computer modeling of the animals movement. Physical examination of fossilized footprints confirmed the results.These are clearly active, agile animals, said Smithsonian paleontologist Hans Sues, who was not part of the study.
''', "Simple text", "english"))
	texts.append(Text('''Вы знаете хотя бы одного человека, который бы не любил слушать музыку? Таких, наверное, не существует. И не удивительно! Ведь музыка создается для того, чтобы дарить эмоции. К примеру, классическая музыка успокаивает, рок позволяет избавиться от накопившейся агрессии. В связи с этим и ассортимент музыкальных новинок с каждым годом увеличивается. Все ли они приносят нам пользу? На самом деле, каждая песня может быть нужной в разные периоды нашей жизни. К тому же, доказано, что жанр музыки, которую мы слушаем, влияет на наше мировосприятие. Согласитесь, если скачать Калинка Малинка и включить на полную громкость, то и настроение сразу улучшится в разы, а все трудности останутся на втором месте.
''', "rus_1", "russian"))
	texts.append(Text('''Когда человек сознательно или интуитивно выбирает себе в жизни какую-то цель, жизненную задачу, он невольно дает себе оценку. По тому, ради чего человек живет, можно судить и о его самооценке - низкой или высокой.

Если человек живет, чтобы приносить людям добро, облегчать их страдания, давать людям радость, то он оценивает себя на уровне этой своей человечности. Он ставит себе цель, достойную человека.

Только такая цель позволяет человеку прожить свою жизнь с достоинством и получить настоящую радость. Да, радость! Подумайте: если человек ставит себе задачей увеличивать в жизни добро, приносить людям счастье, какие неудачи могут его постигнуть? Не тому помочь? Но много ли людей не нуждаются в помощи?

Если жить только для себя, своими мелкими заботами о собственном благополучии, то от прожитого не останется и следа. Если же жить для других, то другие сберегут то, чему служил, чему отдавал силы.''', "rus_2", "russian"))
	for text in texts:
		analyse_text(text)

def analyse_text(text):
	prob, mse = text_processing(text)
	prob["diff"] = np.abs(prob["prob"] - prob["cipf"])
	sns.lineplot(data=prob,x=prob.index, y="prob", label="prob")
	sns.lineplot(data=prob,x=prob.index, y="cipf", label="cipf")
	plt.savefig(f"{text.name}_prob_cipf.png")
	plt.clf()

	sns.lineplot(data=prob,x=prob.index, y="diff", label="diff")
	plt.savefig(f"{text.name}_diff_prob_cipf.png")
	plt.clf()

	print("mse ", mse)
	print(prob.iloc[:20])


if __name__ == "__main__":
	main()